---
layout: handbook-page-toc
title: "Data Team - How We Work"
description: "GitLab Data Team Workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

## <i class="fas fa-chart-line fa-fw color-orange font-awesome" aria-hidden="true"></i> Data Analysis Process
{: #data-analysis-process}

Analysis usually begins with a question.
A stakeholder will ask a question of the data team by creating an issue in the [Data Team project](https://gitlab.com/gitlab-data/analytics/) using the appropriate template.
The analyst assigned to the project may schedule a discussion with the stakeholder(s) to further understand the needs of the analysis, though the preference is always for async communication.
This meeting will allow for analysts to understand the overall goals of the analysis, not just the singular question being asked, and should be recorded.
All findings should be documented in the issue.
Analysts looking for some place to start the discussion can start by asking:
* How can your favorite reports be improved?
* How do you use this data to make decisions?
* What decisions do you make and what information will help you to make them quicker/better?

An analyst will then update the issue to reflect their understanding of the project at hand.
This may mean turning an existing issue into a meta issue or an epic.
Stakeholders are encouraged to engage on the appropriate issues.
The issue then becomes the SSOT for the status of the project, indicating the milestone to which it has been assigned and the analyst working on it, among other things.
The issue should always contain information on the project's status, including any blockers that can help explain its prioritization.
Barring any confidentiality concerns, the issue is also where the final project will be delivered, after peer/technical review.
When satisfied, the analyst will close the issue.
If the stakeholder would like to request a change after the issue has been closed, s/he should create a new issue and link to the closed issue.

The Data Team can be found in the #data channel on slack.

### Working with the Data Team

1. Once a KPI or other Performance Indicate is defined and assigned a prioritization, the metric will need to be added to Sisense by the data team.
2. **Before** syncing with the data team to add a KPI to Sisense, the metric must be:
    * Clearly defined in the relevant section in the handbook and added to the GitLab KPIs [with all of its parts](/handbook/ceo/kpis/#parts-of-a-kpi).
    * Reviewed with the Financial Business Partner for the group.
    * Approved and reviewed by the executive of the group.
3. Once the KPI is ready to be added into Sisense, create an issue on the [GitLab Data Team Issue Tracker](https://gitlab.com/gitlab-data/analytics/issues) using the [KPI Template](https://gitlab.com/gitlab-data/analytics/blob/master/.gitlab/issue_templates/KPI%20Template.md) or [PI Request Template](https://gitlab.com/gitlab-data/analytics/blob/master/.gitlab/issue_templates/Visualization%20or%20Dashboard-%20New%20Request.md).
    * The Data team will verify the data sources and help to find a way to automate (if necessary).
    * Once the import is complete, the data team will present the information to the owner of the KPI for approval who will document in the relevant section of the handbook.

### SLO for Issues and Merge Requests 
* First-Response SLO for a new Issue or MR: 36 hours from the time of creation
* Issue Close SLO is based on the [Issue Weight](https://about.gitlab.com/handbook/business-ops/data-team/how-we-work/#issue-pointing) assigned by the Data Team.
  * Issue weight of 1-5 points: 4 weeks (2 milestones)
  * Issue weight > 5 points: No SLO
* MR Review SLO
  * 4 weeks (2 milestones)

### Request to Expedite Responses
Requests to expedite responses, triage issues, or review MRs is rare. Given the Data Team's shared-service model, expediting an item is asking to de-prioritize other work. To request an expedited response:
1. Confirm there is a valid reason for moving your request ahead of others.
1. Post a note to #data, along with a link to your Issue and a reason why you need an expedited response. Please do not DM an individual on the Data Team directly.
1. A member of the Data Team will respond within 1 business day.

### Can I get an update on my dashboard?

The data team's priorities come from our OKRs.
We do our best to service as many of the requests from the organization as possible.
You know that work has started on a request when it has been assigned to a milestone.
Please communicate in the issue about any pressing priorities or timelines that may affect the data team's prioritization decisions.
Please do not DM a member of the data team asking for an update on your request.
Please keep the communication in the issue.

## <i class="fas fa-tasks fa-fw icon-color font-awesome" aria-hidden="true"></i>How we Work

### Documentation

The data team, like the rest of GitLab, works hard to document as much as possible. We believe [this framework](https://www.divio.com/blog/documentation/) for types of documentation from Divio is quite valuable. For the most part, what's captured in the handbook are tutorials, how-to guides, and explanations, while reference documentation lives within in the primary analytics project. We have aspirations to tag our documentation with the appropriate function as well as clearly articulate the [assumed audiences](https://v4.chriskrycho.com/2018/assumed-audiences.html) for each piece of documentation.

### Prioritization

As a central shared service with finite time and capacity and with a responsibility to operate and develop the company's central Enterprise Data Warehouse, the Data Team must focus its time and energy on initiatives that will yield the greatest positive impact to the [overall global organization](https://about.gitlab.com/handbook/values/#global-optimization) towards [improving customer results](https://about.gitlab.com/handbook/values/#customer-results).

The Data Team uses a **Value Calculator** to quantify the business value of new initiatives (issue, epic, OKR, strategic project) to enable prioritization and ranking of the Data Team development queue. The Value Calculator provides a uniform and transparent mechanism for ranking and enables all work to be evaluated on equal terms. The value calculator approach is similar to the [RICE Scoring Model](https://www.productplan.com/glossary/rice-scoring-model/) for Product Managers and the [Demand Metric Prioritization Model](https://blog.demandmetric.com/2009/02/06/prioritize-your-strategic-initiatives/) for Marketing.

### Standing Priorities

Every day in Data brings a new challenge or opportunity. However, The Data Team strives to spend the majority of its time developing and operating the Enterprise Data Warehouse and related systems, keeping fresh data flowing through the system, regularly expanding the breadth of data available for analysis, and delivering high-impact strategic projects. Our standing priorities are listed below.

| Rank | Priority | Description | Allocation We Aspire To |
| ------ | ------ | ------ | ------ |
| 1 | Production Operations | Activities required to maintain efficient and reliable data services, including triage, bug fixes, and patching to meet established [Service Level Objectives](/handbook/business-ops/data-team/platform/#slos-service-level-objectives-by-data-source). | 10-20%, though will fluctuate as driven by incident frequency and complexity | 
| 2 | Data Team OKRs | The Data Team identifies 3-4 strategic-level OKRs per quarter, primarily focused on core infrastructure and data development that will be beneficial to the entire company, aligned with CEO and Finance OKRs. | 60-75%, though this will fluctuate as driven by larger Functional Team OKRs |
| 3 | Other work | Other work, including Functional Team OKRs, as prioritized and ranked using the Value Calculator.| 15-25% |

We use [scoped labels in GitLab](https://gitlab.com/groups/gitlab-data/-/labels?utf8=%E2%9C%93&subscribed=&search=priority%3A%3A) to track our issues across these priorities. 

### Resolving Prioritization and Blocking Issues

In rare situations [established SLOs](https://about.gitlab.com/handbook/business-ops/data-team/how-we-work/#slo-for-issues-and-merge-requests) do not meet turnaround needs and in these cases the Data Team provides an [expedite response](https://about.gitlab.com/handbook/business-ops/data-team/how-we-work/#request-to-expedite-responses) capability. The Data Team will provide an date estimate if expedited request cannot be handled per the expedite response SLO.

### Data Team Value Calculator

The calculator below is based on the following [Value Calculator](https://docs.google.com/spreadsheets/d/1FROB7j0YfNS_cQM6qQD0CPkE0Emuf77EJwTb96UduWs/edit?usp=sharing) spreadsheet. Please select the values below to define the value of new work.

<%= partial 'includes/data_team_value_calculator_vue' %>

### OKR Planning

The Data Team OKRs aspire to align with Business Operations OKRs, Finance Division OKRs, and CEO OKRs, thereby aligning with the OKRs of the Divisions we support. Due to the nature of the the technical and data infrastructure work required to develop and operate an Enterprise Data Warehouse this will not always be the case.

At the beginning of a FQ, the Data Team will outline all actions that are required to succeed with our KRs *and* in helping other teams measure the success of their KRs.
The best way to do that is via a team brain dump session in which everyone lays out all the steps they anticipate for each of the relevant actions.
This is a great time for the team to raise any blockers or concerns they foresee.
These should be recorded for future reference.

These OKRs drive ~60% of the work that the central data team does in a given quarter.
The remaining time is divided between urgent issues that come up and ad hoc/exploratory analyses.
Specialty data analysts (who have the title "Data Analyst, Specialty") should have a similar break down of planned work to responsive work, but their priorities are set by their specialty manager.

Examples of OKR alignment *in-action* includes:
1. [FY21-Q2 CEO](https://about.gitlab.com/company/okrs/fy21-q2/) Objective 2, aligns to [Data Team Objective 2](https://gitlab.com/groups/gitlab-com/business-ops/-/epics/80) for delivering automated data pipelines in support of Growth initiatives.
1. [FY21-Q2 CEO](https://about.gitlab.com/company/okrs/fy21-q2/) Objective 3, KR4 and KR5 align to [Data Team Objective 1](https://gitlab.com/groups/gitlab-com/business-ops/-/epics/79) to deliver a Finance ARR Data Mart.

### Milestone Planning

The data team currently works in two-week intervals, called milestones.
Milestones start on Tuesdays and end on Mondays.
This discourages last-minute merging on Fridays and allows the team to have milestone planning meetings at the top of the milestone.

Milestones may be three weeks long if they cover a major holiday or if the majority of the team is on vacation or at Contribute.
As work is assigned to a person and a milestone, it gets a weight assigned to it.

Milestone planning should take into consideration:
* vacation timelines
* conference schedules
* team member availability
* team member work preferences (specialties are different from preferences)

The milestone planning is owned by the Manager, Data. 

The timeline for milestone planning is as follows:
* Meeting Preparation - Responsible Party: Milestone Planner
   * Investigate and flesh out open issues.
   * Assign issues to the milestone based on alignment with the Team Roadmap.
   * Note: Issues are not assigned to an individual at this stage, except where required.

| Day | Current Milestone | Next Milestone |
| ---- | ---- | ---- |
| 0 - 1st Wednesday |  **Milestone Start** <br><br>[Roll Milestone](https://gitlab.com/gitlab-data/analytics/issues/new?issuable_template=Milestone%20Rolling) | - |
| 7 - 2nd Tuesday | **Midpoint** <br><br>Any issues that are at risk of slipping from the milestone must be raised by the assignee | - | 
| 10 - 2nd Friday | **The last day to submit MRs for review** <br><br>MRs must include documentation and testing to be ready to merge <br><br>No MRs are to be merged on Fridays | **Milestone is roughly final** <br><br>Milestone Planner verifies issue priority and team capacity for next milestone. |
| 13 - 2nd Monday | **Last day of Milestone** <br><br>Ready MRs can be merged | - |
| 14 - 2nd Tuesday | **Meeting Day** <br><br> All unfinished issues either need to be removed from milestones or rolled to the next | **Milestone Planning** <br><br> Scheduled DE meeting with a tactical discussion of the work to be completed next Milestone. Stakeholders and submitters are updated with what will or wont be added to the next milestone. |

The short-term goal of this process is to improve our ability to plan and estimate work through better understanding of our velocity.
In order to successfully evaluate how we're performing against the plan, any issues not raised at the T+7 mark should not be moved until the next milestone begins.

The work of the data team generally falls into the following categories:
* Infrastructure
* Analytics
   * Central Team
   * Specialist Team
* Housekeeping

During the milestone planning process, we point issues.
Then we pull into the milestone the issues expected to be completed in the timeframe.
Points are a good measure of consistency, as milestone over milestone should share an average.
Then issues are prioritized according to these categories.

Issues are not assigned to individual members of the team, except where necessary, until someone is ready to work on it.
Work is not assigned and then managed into a milestone.
Every person works on the top priority issue for their job type.
As that issue is completed, they can pick up the next highest priority issue.
People will likely be working on no more than 2 issues at a time.

Given the power of the [Ivy Lee](https://jamesclear.com/ivy-lee) method, this allows the team to collectively work on priorities as opposed to creating a backlog for any given person.
As a tradeoff, this also means that every time a central analyst is introduced to a new data source their velocity may temporarily decrease as they come up to speed;
the overall benefit to the organization that any analyst can pick up any issue will compensate for this, though.
Learn [how the product managers groom issues](https://www.youtube.com/watch?v=es-SuhU_6Rc).

Data Engineers will work on Infrastructure issues.
Data Analysts, Central and sometimes Data Engineers work on general Analytics issues.
Data Analysts, <Specialty> work on <Specialty> analyses, e.g Growth, Finance, etc.

There is a demo of [what this proposal would look like in a board](https://gitlab.com/groups/gitlab-data/-/boards/1187725).

This approach has many benefits, including:
1. It helps ensure the highest priority projects are being completed
2. It can help leadership identify issues that are blocked
3. It provides leadership view into the work of the data team, including specialty analysts whose priorities are set from outside the data function
4. It encourages consistent [throughput](/handbook/engineering/management/throughput/) from team members
5. It makes clear to stakeholders where their ask is in priority
6. It helps alleviate the pressure of planning the next milestone, as issues are already ranked

### Issue Types

There are three *general* types of issues:
* Discovery
* Introducing a new data source
* Work

Not all issues will fall into one of these buckets but 85% should.

##### Discovery issues
Some issues may need a discovery period to understand requirements, gather feedback, or explore the work that needs to be done.
Discovery issues are usually 2 points.

##### Introducing a new data source
Introducing a new data source requires a *heavy lift* of understanding that new data source, mapping field names to logic, documenting those, and understanding what issues are being delivered.
Usually introducing a new data source is coupled with replicating an existing dashboard from the other data source.
This helps verify that numbers are accurate and the original data source and the data team's analysis are using the same definitions.

##### Work
This umbrella term helps capture:
* inbound requests from GitLab team-members that usually materialize into a dashboard
* housekeeping improvements/technical debt from the data team
* goals of the data team
* documentation notes

It is the responsibility of the assignee to be clear on what the scope of their issue is.
A well-defined issue has a clearly outlined problem statement. Complex or new issues may also include an outline (not all encompassing list) of what steps need to be taken.
If an issue is not well-scoped as its assigned, it is the responsibility of the assignee to understand how to scope that issue properly and approach the appropriate team members for guidance early in the milestone.

### Workflow Summary 

| Stage (Label)               | Responsible        | Description                                          | Completion Criteria                                                                                                    |
|-----------------------------|--------------------|------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------|
| `workflow::1 - triage`      | Data               | New issue, being assessed                            | Item has enough information to enter problem validation.                                                               |
| `workflow::2 - validation`  | Data, Business DRI | Clarfying issue scope and prosing solution           | Solution defined with sign off from business owners on proposed solution that is valuable, usable, viable and feasible |
| `workflow::3 - scheduling`  | Data               | Waiting for scheduling                               | Item has a numerical milestone label                                                                                   |
| `workflow::4 - scheduled`   | Data               | Waiting for development                              | Data team has started development                                                                                      |
| `workflow::5 - development` | Data               | Solution is actively being developed                 | Initial engineering work is complete and review process has started                                                    |
| `workflow::6 - review`      | Data               | Waiting for or in Review                             | MR(s) are merged. Issues had all conversations wrapped up.                                                             |
| `workflow::X - blocked`     | Data, Business DRI | Issue needs intervention that assignee can't perform | Work is no longer blocked                                                                                              |

Generally issues should move through this process linearly. Some templated issues will skip from `triage` to `scheduling` or `scheduled`.

#### Issue Pointing

**Issue pointing captures the complexity of an issue, not the time it takes to complete an issue. That is why pointing is independent of who the issue assignee is.**

* Refer to the table below for point values and what they represent.
* We size and point issues as a group.
* Effective pointing requires more fleshed out issues, but that requirement shouldn't keep people from creating issues.
* When pointing work that happens outside of the Data Team projects, add points to the issue in the relevant Data Team project and ensure issues are cross-linked.

| Weight | Description |
| ------ | ------ |
| Null | Meta and Discussions that don't result in an MR |
| 0 | Should not be used. |
| 1 | The simplest possible change including documentation changes. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A typical change, with understood requirements but some complicating factors|
| 5 | A more complex change. Requirements are *probably* understood or there might be dependencies outside the data-team. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. |
| 13 | A significant change that has dependencies and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break into smaller Issues. |

#### Issue Labeling

Think of each of these groups of labels as ways of bucketing the work done. All issues should get the following classes of labels assigned to them:

* **Who** (Purple): Team for which work is primarily for (Data, Finance, Sales, etc.)
* **When**: [Workflow Status](https://gitlab.com/groups/gitlab-data/-/labels?utf8=%E2%9C%93&subscribed=&search=workflow) and [Prioritization Type](https://gitlab.com/groups/gitlab-data/-/labels?utf8=%E2%9C%93&subscribed=&search=Priority+Label%3A+)
* **Where** (Brown): Which part of the team performs the work (Analytics, Infrastructure, Housekeeping)
* **What** - Data or Tool
  * Data (Light Green): Data being touched (Salesforce, Zuora, Zendesk, GitLab.com, etc.)
  * Tool (Light Blue) (Sisense, dbt, Stitch, Airflow, etc.)
* **How** (Orange): Type of work (Documentation, Break-fix, Enhancement, Refactor, Testing, Review)

Optional labels that are useful to communicate state or other priority
* State (Red) (Won't Do, Blocked, Needs Consensus, etc.)

* Inbound: For issues created by folks who are not on the data team; not for asks created by data team members on behalf of others

#### Workflows

##### Merge Request Workflow

*Ideally*, your workflow should be as follows:
1. Confirm you have access to the analytics project. If not, request Developer access so you can create branches, merge requests, and issues.
1. Create an issue, open an existing issue, or assign yourself to an existing issue. The issue is assigned to the person(s) who will be doing the work.
1. Add appropriate labels to the issue (see above)
1. Open an MR from the issue using the "Create merge request" button. This automatically creates a unique branch based on the issue name. This marks the issue for closure once the MR is merged.
1. Push your work to the branch
1. Update the MR with an [appropriate template](https://gitlab.com/gitlab-data/analytics/-/tree/master/.gitlab%2Fmerge_request_templates). Our current templates are:
  * **dbt Model Changes** - used for any change involving dbt. Analysts will most often use this one
  * **add_manifest_tables** - for adding tables to pgp extract
  * **periscope** - for getting a Periscope dashboard reviewed
  * **python_changes** - for general changes to Python code
  * **All Other Changes** - for work that doesn't generally fall into these categories
1. Run any relevant jobs to the work being proposed
  * e.g. if you're working on dbt changes, run the job most appropriate for your changes. See the [CI jobs page](/handbook/business-ops/data-team/platform/ci-jobs/) for an explanation of what each job does.
1. Document in the MR description what the purpose of the MR is, any additional changes that need to happen for the MR to be valid, and if it's a complicated MR, how you verified that the change works. See [this MR](https://gitlab.com/gitlab-data/analytics/merge_requests/658) for an example of good documentation. The goal is to make it easier for reviewers to understand what the MR is doing so it's as easy as possible to review.
1. Assign the MR to a peer to have it reviewed. If assigning to someone who can merge, either leave a comment asking for a review without merge, or you can simply leave the `WIP:` label.
  * Note that assigning someone an MR means action is required from them.
  * The peer reviewer should use the native approve button in the MR after they have completed their review and approve of the changes in the MR.
  * Adding someone as an approver is a way to tag them for an FYI. This is similar to doing `cc @user` in a comment.
  * After approval, the peer reviewer should send the MR back to the author to decide what needs to happen next. The reviewer should not be responsible for the final tasks. The author is responsible for finalizing the checklist, closing threads, removing WIP, and getting it in a merge-ready state.
1. Once it's ready for further review and merging, remove the `WIP:` label, mark the branch for deletion, mark squash commits, and assign to the project's maintainer. Ensure that the attached issue is appropriately labeled and pointed.

Other tips:
1. The Merge Request Workflow provides clear expectations; however, there is some wiggle room and freedom around certain steps as follows.
  * For simple changes, it is the MR author who should be responsible for closing the threads. If there is a complex change and the concern has been addressed, either the author or reviewer could resolve the threads if the reviewer approves.
1. Reviewers should have 48 hours to complete a review, so plan ahead with the end of the milestone.
1. When possible, questions/problems should be discussed with your reviewer before submitting the MR for review. Particularly for large changes, review time is the least efficient time to have to make meaningful changes to code, because you’ve already done most of the work!


### YouTube

We encourage everyone to record videos and post to GitLab Unfiltered. The [handbook page on YouTube](/handbook/communication/youtube/#post-everything) does an excellent job of telling why we should be doing this. If you're uploading a video for the data team, be sure to do the following extra steps:

* Add `data` as a video tag
* Add it to the [Data Team playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI)
* Share the video in #data channel on slack
