---
layout: handbook-page-toc
title: "Technical Evangelism"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goal
To build GitLab's technical brand with deep, meaningful conversations on engineering topics relevant to our industry by leveraging our community of team-members and the broader ecosystem.

### What do we do?

The GitLab technical evangelists are engineers who enjoy learning and teaching the latest technology topics with the wider community. The evangelists are supported by program management within the team and also collaborate closely with the content and PR teams in corporate marketing to amplify their voices. There are two specific goals for the team:

1. **Thought leadership:** Drive GitLab awareness and brand value as full-time technical evangelists participating in thought leadership on au courant topics in the cloud native and cloud computing ecosystem. 

2. **Ecosystem engagement:** Establish GitLab's technical thought leadership by building GitLab's influence in the tech community through participation in open source projects, foundations, and other consortiums. A description of consortiums can be found in our [Open Source Program handbook page](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships).

3. **Marketing value:** Work with other teams in GitLab (such as content, social, marketing programs) to repurpose, maximize the value of, and measure the impact of content created for thought leadership and ecosystem engagement.


## Who we are

We are members of the [Community Relations team](/handbook/marketing/community-relations/).

### Team members and focus areas

1. [Abubakar Siddiq Ango](/company/team/#abuango) - Technical Evangelism Program Manager
    - Program management
        - Running the CFP process for conferences on the [Technical Evagelism events list](https://docs.google.com/spreadsheets/u/1/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit?usp=drive_web&ouid=101407496565734994973)
        - Organizing Technical evangelism team's content creation and repurposing efforts
    - Kubernetes
    - Cloud Native
    - Language skills: English, Yoruba, Hausa
2. [Brendan O'Leary](/company/team/#brendan) -  Senior Developer Evangelist
    - DevOps with a focus on the application developer perspective
        - SCM
        - GitOps
        - CI
        - .NET and Javascript communities
    - Language skills: English
3. [Michael Friedrich](/company/team/#dnsmichi) - Developer Evangelist
    - DevOps with a focus on the SRE, Ops engineers' perspective
        - CI/CD
        - Serverless
        - Observability
    - Language skills: English, German, Austrian

## How we work

### Team Bookmarks
* CFP
    * [Create CFP Meta Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta)
    * [Create CFP Submission Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission)
* [Create TE Request Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=technical-evangelist-request)
* [Events Spreasheet](https://docs.google.com/spreadsheets/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit#gid=0)
* [Events Calendar](#our-events-list)
* [Team Shared Drive](https://drive.google.com/drive/u/0/folders/0AEUOlCStMBC9Uk9PVA) (Internal)
* [Weekly Meeting Agenda](https://docs.google.com/document/d/1oPXtlWNDbeut-dbFDLXBCfiBJLWwbzjjK9CFu5o8QzU/edit#) (Internal)
* [Team Collab Session Agenda Doc](https://docs.google.com/document/d/1Hs2DrUf9QJQR8fSsNO9WgX7c9sS46NSjEDuKHrrvPv0/edit#heading=h.vgcz1npa9iy) (Internal)
* Team Issue Boards
    * [General](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name[]=tech-evangelism)
    * [CFP](https://gitlab.com/groups/gitlab-com/-/boards/1616902?&label_name[]=TE-CFP)
    * [Content](https://gitlab.com/groups/gitlab-com/-/boards/1624080?&label_name[]=tech-evangelism)
    * [Milestones](https://gitlab.com/groups/gitlab-com/-/boards/1672643?label_name[]=tech-evangelism)

### Projects
Our team maintains many projects to help show off technical concepts, engage with communities, provide examples of using GitLab with other technologies, and automate our team processes.  See [Technical Evangelism Projects](/handbook/marketing/community-relations/technical-evangelism/projects/) for a list of all of those projects.

### CFPs
The Technical Evangelism team has several interactions with "CFPs" - calls for proposals for speaking at conferences. Firstly, many of our Technical Evangelists directly contribute by speaking [at conferences themselves](#requesting-a-technical-evangelist-to-submit-a-CFP).  In addition, our program team [manages responses to CFPs](/handbook/marketing/community-relations/technical-evangelism/cfps/) for GitLab through our issue boards.

### Issue Tracker
We work in the open using the GitLab's [Corporate Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues). We own the label `tech-evangelism` which can be applied to issues in any project under the [gitlab-com](https://gitlab.com/gitlab-com) and [gitlab-org](https://gitlab.com/gitlab-org) parent groups. 

You can follow the latest our team is working on by looking at [our label-based issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name[]=tech-evangelism).

### Issue Labels
Using the [tech-evangelism](https://gitlab.com/groups/gitlab-com/-/labels?search=tech-evangelism) label on an issue means we are working on it or participating in the ongoing conversation. Team members are subscribed to issue/MR updates for this label.

The `tech-evangelism` label is accompanied by some labels we use to organize our work.

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> You only need to use the `tech-evangelism` label on issues requiring the attention of the Technical Evangelism team. Other labels are applied by the team as their state change or as contained in various issue templates.
{: .alert .alert-info}

#### General Labels

| **CFP Labels** |  **Description** |
| ------ | ------ |
| `TE-DueSoon` |  This is used to monitor TE issues that are due soon | 
| `TE-Peer-Review` |  Feedback is needed on the issue from TE team members |
| `TE-Ops` | Used to label issues related to the technical evangelism `Ops in DevOps` theme |
| `TE-Dev` | Used to label issues related to the technical evangelism `Dev in DevOps` theme |
| `TE-k8s` | Used to label issues related to the technical evangelism `Kubernetes` theme |


#### Technical Evangelism Team Administrative Processes
The team creates issues for iteration, team discussions, and other issues for internal processes. These issues are tracked using the following labels:

| **Process Labels** | **Description** |
| ------ |  ------ |
| `TE-Process::Open` | Process related issues that are still being discussed or worked |
| `TE-Process::Pending` | Process related issues on hold due to an external factor |
| `TE-Process::Done` | Completed Process issues |
| `TE-Process::FYI` | Issues that require no action from the team, but need to be aware of |

### Request a Technical Evangelist
If you require a Technical Evangelist's help for anything, please use this ["Technical Evangelist Request" issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=technical-evangelist-request) to create an issue, and we will get back to as soon as possible. On creating the request issue, two labels are applied: `TE-Request::New` & `TE-Request-Type::External` and transitioned through different states as listed below:

| **Request Labels** | **Description** |
| ------ |  ------ |
| `TE-Request::New` | Newly created requests |
| `TE-Request::Open` | Requests that are still being worked on or considered |
| `TE-Request::Done` | Completed requests |
| `TE-Request::Cancelled` | Requests not considered by the team |
| `TE-Request-Type::External` | Identifies Requests created by other teams |
| `TE-Request-Type::internal` | Identifies Requests created by Technical Evangelism team members |

#### CFPs (Call for Proposals)
See [see Requesting a Technical Evangelist to submit a CFP](/handbook/marketing/community-relations/technical-evangelism/cfps/) to request a Technical Evangelist to submit to a CFP for a corporate, field, or partner event.


#### Content Creation 

Covers any content request, Webcast, Interview, Meetup, etc. These content issues first get the parent `tech-evangelism` label applied, and are then transitioned through different stages using the scoped labels below:

| **CFP Labels** |  **Description** |
| ------ | ------ |
| `TE-Content::new-request` |  New content request | 
| `TE-Content::draft` |  A member of the TE team is working on the content |
| `TE-Content::review` |  Content Author is seeking review from other team memebers| 
| `TE-Content::published` |   Content has been published |
| `TE-Content::Repurposing` |  Content is being repurposed on other platforms or mediums | 
| `TE-Content::cancelled` | Content is no longer needed, and no one is working on it | 

```plantuml
start
: tech-evangelism, TE-Content::new-request;
if (Content Request Cancelled at any stage) then (yes)
     :tech-evangelism, TE-Content::cancelled;
else
    : tech-evangelism, TE-Content::draft;
    : tech-evangelism, TE-Content::review;
    : tech-evangelism, TE-Content::published;
    : tech-evangelism, TE-Content::Repurposing;
endif

stop
```

### Slack
GitLab team members can also reach us at any time on the [#tech-evangelism](https://app.slack.com/client/T02592416/CMELFQS4B) Slack channel where we share updates, ideas, and thoughts with each other and the wider team.



## Useful links
1. [How to be an evangelist](/handbook/marketing/community-relations/technical-evangelism/how-to-be-an-evangelist/)
2. [How to submit a successful conference proposal](/handbook/marketing/community-relations/technical-evangelism/writing-cfps/)
3. [Consortiums we work with](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships)
4. [Speaking logistics](/handbook/marketing/community-relations/technical-evangelism/speaking-logistics/)
