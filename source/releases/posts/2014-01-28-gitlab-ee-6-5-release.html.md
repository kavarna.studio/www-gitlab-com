---
title: GitLab Enterprise Edition 6.5 Release
date: January 28, 2014
author: Jacob Vosmaer
categories: releases
---

Today we announce the release of GitLab Enterprise Edition 6.5. 
GitLab is an open source code hosting and project management application.
This release combines all the [improvements in GitLab Community Edition 6.5](/blog/2014/01/21/gitlab-ce-6-dot-5-released/) with the [advanced LDAP and project sharing features of GitLab Enterprise Edition](https://www.gitlab.com/gitlab-ee/).

Our [subscribers](https://www.gitlab.com/subscription/) can download GitLab 6.5 Enterprise Edition from [GitLab Cloud](https://gitlab.com).
