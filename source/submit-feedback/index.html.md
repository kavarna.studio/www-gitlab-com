---
layout: markdown_page
title: GitLab Feedback
description: Submit feedback on GitLab - feature requests, bugs, and what we could be doing better.
---


## Help topics
{:.no_toc}

- TOC
{:toc}

---

## Feature proposals

Feature proposals should be submitted to the issue tracker of the relevant product:

* [GitLab Issue Tracker](https://gitlab.com/gitlab-org/gitlab/issues)

Please read the [contributing guidelines for feature proposals](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#feature-proposals) before posting on the issue tracker and make use of the "Feature proposal" issue template.

## Reproducible bugs

Bug reports should be submitted to the issue tracker of the relevant product:

* [GitLab Issue Tracker](https://gitlab.com/gitlab-org/gitlab/issues)

Please read the [contributing guidelines for reporting bugs](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#issue-tracker-guidelines) before posting on the issue tracker and make use of the "Bug" issue template.

Other resources for discussion:

* [#gitlab IRC channel](https://webchat.freenode.net/?channels=gitlab): a Freenode channel to get in touch with other GitLab users and get help. It is managed by James Newton (newton) and Drew Blessing (dblessing).
* [GitLab Community Forum](https://forum.gitlab.com/): this is the best place to have a discussion.
* [Mailing list](https://groups.google.com/forum/#!forum/gitlabhq): please search for similar issues before posting your own, there's a good chance somebody else had the same issue you have now and has resolved it.
* [Gitter chat room](https://gitter.im/gitlabhq/gitlabhq#): here you can ask questions when you need help.

## Research

We conduct research throughout all product stages, both to see how existing features are working and to uncover what to build in the future.

* [Sign up](https://gitlab.fra1.qualtrics.com/jfe/form/SV_51J9D8skLbWqdil?Source=submit-feedback) to be invited to participate in research studies in your area of interest. 
* [Read more](/community/gitlab-first-look/) about research at GitLab. 

## Company feedback

For feedback specific to GitLab the company, feel free to open a [merge request in our publicly available handbook](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests) if you have a specific change in mind. For questions or discussion, please open a new [issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues) and we'll be happy to have a conversation.
