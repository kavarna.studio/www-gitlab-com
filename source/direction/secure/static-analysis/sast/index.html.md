---
layout: markdown_page
title: "Category Direction - Static Application Security Testing (SAST)"
description: "Static application security testing (SAST) checks the source code to find possible vulnerabilities in the implementation. Learn more here!"
canonical_path: "/direction/secure/static-analysis/sast/"
---

- TOC
{:toc}

## Description

### Overview

Static Application Security Testing (SAST) checks source code to find possible security vulnerabilities. SAST helps developers identify weaknesses and security issues earlier in the software development lifecycle, before code is deployed. SAST usually is performed when code is being submitted to a code repository. Think of it as a spell checker for security issues.

SAST is performed on source code or binary files and thus usually won't require code to be compiled, built, or deployed. However, this means that SAST cannot detect runtime or environment issues. SAST can analyze the control flow, the abstract syntax tree, how functions are invoked, and if there are information leaks to detect weak points that may lead to unintended behaviors.

Just like spell checkers, SAST analyzers are language and syntax specific and can only identify known classes of issues. SAST does not replace code reviewers, instead, it augments them, and provides another line of proactive defense against common and known classes of security issues. SAST is specifically about identifying potential security issues, so it should not be mistaken for [Code Quality](https://about.gitlab.com/direction/verify/code_quality/).

Security tools like SAST are best when integrated directly into the [Devops Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/) and every project can benefit from SAST scans, which is why we include it in [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/). 

> GitLab was recently named as a [Niche player in the 2020 Gartner Quadrant for Application Security Testing](https://about.gitlab.com/press/releases/2020-05-11-gitlab-positioned-niche-players-quadrant-2020-gartner-magic-quadrant-application-security-testing.html). 

### Goal

Overall we want to help developers write better code and worry less about common security mistakes. SAST should help *prevent* security vulnerabilities by helping developers easily *identify* common security issues as code is being contributed and *mitigate* proactively.  SAST should *integrate* seamlessly into a developer’s workflow because security tools that are actively used are effective.

* *Prevent* - find common security issues as code is being contributed and before it gets deployed to production.
* *Identify* - continuously monitor source code for known or common issues.
* *Mitigate* - make it easy to remediate identified issues, automatically if possible.
* *Integrate* - integrate with the rest of the DevOps pipeline and [play nice with other vendors](https://about.gitlab.com/handbook/product/gitlab-the-product/#plays-well-with-others).

The importance of these goals is validated by GitLab's [2020 DevSecOps Landscape Survey](https://about.gitlab.com/developer-survey/#security). With 3,650 respondents from 21 countries, the survey found: 

* Only 13% of companies give developers access to the results of application security tests.
* Over 42% said testing happens too late in the lifecycle. 
* 36% reported it was hard to understand, process, and fix any discovered vulnerabilities. 
* 31% found prioritizing vulnerability remediation an uphill battle.

### Language Support
We want to make SAST easy to set up and use, making complexity transparent to users where possible. GitLab can automatically detect the programming language of a project and run the appropriate analyzer. We [support a variety of popular languages](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks).

We want to increase language coverage by including support for the most common languages. We look at a variety of sources to determine language priorities including [industry trends](https://insights.stackoverflow.com/survey/2019#technology), [projects hosted on GitLab](https://about.gitlab.com/blog/2020/04/02/security-trends-in-gitlab-hosted-projects/), as well as [analyst reports](https://www.gartner.com/en/documents/3984345/magic-quadrant-for-application-security-testing) (italics below indicate languages called out specifically in analyst reports).

Language priorities (in addition to our existing [language support](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks))
- *Java*
- *C#* (.Net Framework + Core)
- *PHP*
- *JavaScript* (better support for popular frameworks)
- Python
- Go
- Kotlin (Android)
- Swift (Apple)

We are also working on a [generic language-agnostic scanning](https://gitlab.com/groups/gitlab-org/-/epics/3260) approach. While currently experimental, generic scanning presents many opportunities to move faster and put more focus on the security rulesets rather than the implementation of those rules in various scanners. 

*User success metrics*

The following measures would help us know which area of SAST on which to focus:
- Tracking the # of SAST configurations (default, out of date, customized)
- Tracking the # of SAST jobs (increase coverage across repos)
- Tracking the # of issues identified by SAST
- Tracking the # of issues resolved that were identified by SAST
- Diversity of coverage of SAST jobs (language, type of identified issues, severity of issues)

## Roadmap
[The SAST Category Maturity level](https://about.gitlab.com/direction/maturity/#secure) is currently at `Viable`. We plan to mature it to `Complete` by January 2021. 

 - [SAST Direction Epic](https://gitlab.com/groups/gitlab-org/-/epics/527)
 - Next Maturity Milestone Epic: [SAST to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2895)

### What's Next & Why
For the next few releases, we are currently focused on [cleaning up the state of our current scanners](https://gitlab.com/groups/gitlab-org/-/epics/3246) and improving support for additional configurations. As part of this initiative, we are working on creating a new [SAST configuration UI](https://gitlab.com/groups/gitlab-org/-/epics/3247) to help make our [security scanner configuration](https://docs.gitlab.com/ee/user/application_security/sast/#configuration) more approachable and easier to understand. This configuration UI will also lay the framework for us to introduce [individual scanner rule customization](https://gitlab.com/groups/gitlab-org/-/epics/3263). 

We also want to bring our [SAST scanners down to Core](https://gitlab.com/groups/gitlab-org/-/epics/2098) to make them available to the most people as part of our [community stewardship commitment](https://about.gitlab.com/company/stewardship/#promises).

**Why is this important?**

GitLab needs at least a minimum level of coverage in the SAST feature set to check the box for compliance and buyer personas. But further SAST has a very real impact to help the world write better code. If Gitlab provides a basic level of SAST to all repositories on Gitlab, we can meaningfully help protect against the simplest of code security issues. That encourages Gitlab to be the source of security information for repositories. It also provides opportunities to show the breadth of GitLab's feature set, and how that enables more complete and holistic DevOps processes.

**Differentiation**

Gitlab uniquely has opportunities within the entire DevOps lifecycle. We can integrate across different DevSecOps stages leveraging data, insight, and functionality from other steps to enrich and automate based on SAST findings.
We even allow [integration with partners and competitors](https://about.gitlab.com/partners/#security) to ensure flexibility. This allows teams to choose specific SAST solutions that fit their unique needs without GitLab being a constraint. This centers GitLab as the system of control and allows people to [extend and integrate other solutions](https://docs.gitlab.com/ee/development/integrations/secure.html) into the GitLab DevSecOps workflow.

## Maturity Plan

- [SAST to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2895)

## Recent Noteworthy Features
- [13.0 - .Net Framework Support](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/#sast-for-net-framework)
- [12.10 - SAST Support for Air-gapped networks](https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/#enhanced-secure-workflows-for-use-in-offline-environments)

## Competitive Landscape
Many well-known commercial products provide SAST solutions. Most of them support multiple languages and provide limited integration into the development lifecycle.

* Competitors are focused on a few areas:
    * Accuracy, breadth, and scope of identifiable issues
    * More code language support
    * Detection Intelligence (AI/ML)
    * Solution automation

Here are some vendors providing SAST tools:
* [Checkmarx](https://www.checkmarx.com/products/static-application-security-testing)
* [Synopsis](https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html)
* [SonarQube](https://www.sonarqube.org)
* [GitHub Enterprise](https://github.com/features/security)
* [CA Veracode](https://www.veracode.com/products/binary-static-analysis-sast)
* [Fortify](https://software.microfocus.com/en-us/products/static-code-analysis-sast/overview)
* [IBM AppScan](https://www.ibm.com/security/application-security/appscan)

GitLab has a unique position to deeply integrate into the development lifecycle, with the ability to leverage CI/CD pipelines to perform the security tests. There is no need to connect the remote source code repository, or to use a different interface.

We can improve the experience even more, by supporting additional features that are currently present in other tools.

* [Support incremental scans for SAST](https://gitlab.com/gitlab-org/gitlab-ee/issues/9815)
* [Auto Remediation support for SAST](https://gitlab.com/gitlab-org/gitlab-ee/issues/9480)

## Analyst Landscape
We want to engage analysts to make them aware of the security features already available in GitLab. They also perform analysis of vendors in the space and have an eye on the future. We will blend analyst insights with what we hear from our customers, prospects, and the larger market as a whole to ensure we’re adapting as the landscape evolves. 

* [2020 Gartner Magic Quadrant: Application Security Testing](https://about.gitlab.com/resources/report-gartner-mq-ast/)
* [2020 Gartner Research: How to Deploy and Perform Application Security Testing](https://www.gartner.com/en/documents/3982363/how-to-deploy-and-perform-application-security-testing)
* [2020 GitLab DevSecOps Landscape Survey](https://about.gitlab.com/developer-survey/)
* [Gartner Application Security Testing Reviews](https://www.gartner.com/reviews/market/application-security-testing)
* [2019 Forester State of Application Security](https://www.forrester.com/report/The+State+Of+Application+Security+2019/-/E-RES145135)
* [OWASP SAST Tools](https://www.owasp.org/index.php/Source_Code_Analysis_Tools)
* [2019 StackOverflow Developer Survey](https://insights.stackoverflow.com/survey/2019#technology)

## Top Customer Success/Sales Issue(s)
[Full list of SAST issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST)

## Top user issue(s)
* [Full list of SAST issues with Customer Label](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=customer)
* [Full list of SAST issues with internal customer label](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=internal%20customer)

## Top Vision Item(s)
* [SAST Product Vision](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=Product%20Vision%20FY21)

## Related Categories
* [DAST](https://about.gitlab.com/direction/secure/dynamic-analysis/dast/) - Dynamic Application Security Testing
* [Dependency Scanning](https://about.gitlab.com/direction/secure/composition-analysis/dependency-scanning/) - Evaluating Dependency Vulnerabilities 
* [Vulnerability Management](https://about.gitlab.com/direction/secure/vulnerability_management/) - Security Dashboards, Reports, and interacting with Vulnerabilities

Last Reviewed: 2020-05-03
Last Updated: 2020-05-02
