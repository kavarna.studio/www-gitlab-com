---
layout: markdown_page
title: "Category Direction - DevOps Reports"
canonical_path: "/direction/manage/devops-reports/"
---

- TOC
{:toc}

## Manage Stage

| Category | **DevOps Reports** |
| --- | --- |
| Stage | [Manage](https://about.gitlab.com/handbook/product/product-categories/#manage-stage) |
| Group | [Analytics](https://about.gitlab.com/handbook/product/product-categories/#analytics-group) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-07-07` |

### Introduction and how you can help
Thanks for visiting the direction page for DevOps Reports in GitLab. This page is being actively maintained by the Product Manager for the [Analytics group](https://about.gitlab.com/handbook/product/categories/#analytics-group). If you'd like to contribute or provide feedback on our category direction, you can:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/3651).
1. Find issues in this category accepting merge requests. 

### Overview

GitLab has a [extraordinary breadth](https://about.gitlab.com/direction/maturity/#category-maturity) for a single application, allowing users to implement the entire [DevOps Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/) with ten stages of functionality ranging from portfolio planning and management; on the one hand, all the way to monitoring and service desk on the other. Each of these stages offers persona specific dashboards such our [security dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), a dashboard for [monitoring](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#getting-metrics-to-display-on-the-metrics-dashboard) applications, for [auditors and compliance officers](https://docs.gitlab.com/ee/user/compliance/compliance_dashboard/index.html#compliance-dashboard-ultimate), for engineers tracking the progress of [code reviews](https://docs.gitlab.com/ee/user/analytics/code_review_analytics.html#code-review-analytics-starter) or software development leaders who want to understand fluctuations in the [productivity](https://docs.gitlab.com/ee/user/analytics/productivity_analytics.html) their software teams. 


With this breadth, GitLab has the potential to provide a level of insight that few others can match. To help users more fully realize this potential we want to address one important view we currently lack: the ability to pull all of these disparate pieces together into a single comprehensive view that cuts across stage boundaries and allows users to re-mix the metrics available in GitLab into dashboards which meet their specific needs.

The DevOps Reports category aims to address this by providing users the ability to:

* Build dashboards from the data available in GitLab
* Drill-in for additional detail and analysis
* See their own pre-computed / pre-aggregated metrics from external sources within GitLab

In addition, DevOps Reports, provides technical infrastructure, small primitives, for product teams across GitLab to:

* Contribute to a library of metrics
* Leverage pre-built experiences for exploring those metrics so as to make their own offerings richer
* Give their customers a wider array of options for building their own dashboards

![Dev Overview](/images/direction/dev/vsm-framework.png)

### Where we are headed

DevOps Reports is comprised of the following capabilities:

* **[Metrics Library](https://gitlab.com/groups/gitlab-org/-/epics/3653)** &mdash; A curated library of metrics from every part of the DevOps library are available for exploration and analysis.
* **[Report Pages](https://gitlab.com/groups/gitlab-org/-/epics/3214)** &mdash; Users explore any metric in the library, spotting trends and exploring root causes via an interactive chart and data table. 
* **[Dashboards](https://gitlab.com/groups/gitlab-org/-/epics/3707)** &mdash; Re-mix metrics from across the product into a Dashboard and share it with others to focus attention on KPIs that matter for each team.

During our first phase we will focus on creating simple mechanisms that will allow us to quickly build-out a robust Metric's Library. These include re-using and expanding the query capability of our existing Insights feature and a new Metrics APIs for pre-computed and aggregated measures. We will also build out basic capabilities in Report Pages to allow users to explore Metrics from the metrics library, export metrics data to CSV, and embed charts in third-party wikis and dashboards. 

We will also begin enhancing current product capabilities such as the metrics displayed in Group Recent Activity to prove out the power of Report Pages to enhance existing product features by providing drill-down and additional insights.

#### Metrics Library Themes

* **Optimizing Flow** &mdash; In conjunction with our emphasis on [Value Stream Management](https://about.gitlab.com/direction/manage/value_stream_management/), we will implement common DevOps and Value Stream metrics such as the [DORA](https://gitlab.com/gitlab-org/gitlab/issues/37139) metrics and Flow Metrics in addition to general value stream process measures for evaluating the performance of a particular value stream process step or stage, detecting out-of-control queues, process bottlenecks, etc. 

* **Planning & Tracking** &mdash; We aim to assist agile project managers and agile teams with frequently used measures such as team velocity, cumulative flow and eventually predictive analytics to help with [planning](https://about.gitlab.com/direction/manage/planning-analytics/).

* **Develop & Test, Secure & Release** &mdash; Development teams often wish to monitor status of builds, test code coverage, security scan results, defect counts and other measures which are also used by Release Managers to gauge release readiness. 

* **Measures of Value** &mdash; Our existing Prometheus integration provides a powerful capability for tracking the outcome of a continuous delivery release in terms of [business metrics that matter](https://www.youtube.com/watch?time_continue=1&v=oG0VESUOFAI&feature=emb_logo). We hope to build on this with deeper integration to our system for [feature flags](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) and [a/b testing](https://gitlab.com/groups/gitlab-org/-/epics/2966).

* **DevOps Maturity** &mdash; We will begin with [relatively straightforward](https://gitlab.com/gitlab-org/gitlab/issues/193435) measurements of usage and adoption as an evolution of our [DevOps Score](https://docs.gitlab.com/ee/user/instance_statistics/dev_ops_score.html) capability. Eventually leaders will be able to set and specific KPIs and process targets derived from out-of-the box Metrics Library metrics on an executive dashboard.

### What's Next and Why

This is an important area of investment for GitLab which provides immediate value to end-users while also establishing foundations to accelerate our work in [Value Stream Management](https://about.gitlab.com/direction/manage/value_stream_management/).

DevOps Reports will allow us to:

* **Create a consistent user experience** not only from out-of-box and user-defined dashboards themselves, but when drilling-in from other areas of the product where analytics are embedded.
* **Make Analytics easier to find and consume** by reducing the number of separate analytics pages in the product while providing more flexibility and value.  
* **Broaden the range of measures we can offer** by making it easy for everyone to contribute new metrics.
* **Focus future investment on signature experiences** aligned to specific daily interactions where GitLab analytics and value stream thinking can give GitLab users superpowers.
* **Get more value out of analytics product enhancements** by having them apply to every metric offered (reuse).

Specific near term steps in this area include:

* Building [APIs](https://gitlab.com/groups/gitlab-org/-/epics/3105) and patterns that create a metrics library for exploration.
* Creating a single [report page](https://gitlab.com/groups/gitlab-org/-/epics/3214) that can display a wide range of metrics in manner appropriate to each without new front-end coding.
* Build out a [metrics library](https://gitlab.com/groups/gitlab-org/-/epics/3653) from metrics already available in GitLab.
* Configurable [Dashboards](https://gitlab.com/groups/gitlab-org/-/epics/3707).

### Maturity Plan
This category is currently **Minimal**. The next step in our maturity plan is achieving a **Viable** state of maturity.
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

For DevOps Reports to be Viable, development teams must be able to construct dashboards from the most consulted / customer requested metrics and find these provide a level of situational awareness on a daily basis or weekly basis. We will verify this via dogfooding with at least ten development groups inside GitLab. 

Furthermore, we expect the use of report pages to enhance existing capabilities with drill-down experiences to increase traffic to Analytics pages by 10% over the next 9 months and by 200% over the next 18 months.

